use serde::Serialize;

#[derive(Serialize, Debug)]
pub struct Message {
    text: String,
    alt: String,
    tooltip: String,
    class: Option<String>,
    percentage: Option<f64>,
}
 
pub trait StatusMessage {
    fn text(&self) -> String;
    fn alt(&self) -> String;
    fn tooltip(&self) -> String;
    fn percentage(&self) -> Option<f64>;
    fn class(&self) -> Option<String>;
    fn serialize(&self) -> String {
        let msg = Message {
            text: self.text(),
            alt: self.alt(),
            tooltip: self.tooltip(),
            class: self.class(),
            percentage: self.percentage(),
        };
        serde_json::to_string(&msg).unwrap()
    }
}

