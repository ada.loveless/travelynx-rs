mod error;
mod status;
mod waybar;
mod travelynx;
use std::time::{Duration, Instant};

use clap::{Args, Parser, Subcommand};
use waybar::StatusMessage;

fn main_loop(client: impl travelynx::Client) {
    let mut last_update = Instant::now();
    let mut status = client.get();
    let mut delay = Duration::from_secs(60);
    let duration = std::time::Duration::from_secs(1);
    loop {
        if delay < last_update.elapsed() {
            log::info!(
                "Updating status as {:?} < {:?}",
                delay,
                last_update.elapsed()
            );
            last_update = Instant::now();
            status = client.get();
            delay = match status {
                Ok(_) => Duration::from_secs(60),
                Err(error::Error::Network(_)) => Duration::from_secs(5),
                Err(error::Error::Parse(_)) => Duration::from_secs(120),
                Err(error::Error::IO(_)) => Duration::from_secs(60),
            };
            log::debug!("Status was {:?}, set delay to {:?}", status, delay);
        } else {
            log::info!("Last update {:>3}s ago, delay is {:?}", last_update.elapsed().as_secs(), delay);
        }
        match &status {
            Ok(value) => println!("{}", value.serialize()),
            Err(value) => println!("{}", value.serialize()),
        }
        std::thread::sleep(duration);
    }
}

#[derive(Debug, Args)]
struct GlobalArgs {}

#[derive(Debug, Subcommand)]
enum Command {
    /// Status command running with the travelynx API
    Status {
        /// API Secret to use
        #[arg(short, long)]
        secret: Option<String>,
        /// Path to read API Secret from
        #[arg(short, long)]
        file_secret: Option<String>,
        /// Run continuously
        #[arg(short, long)]
        continuous: bool,
    },
    /// Demo mode based on a pre-defined json
    Demo {
        /// Sample file to use
        #[arg(short, long)]
        file: Option<String>,
        /// Run continuously
        #[arg(short, long)]
        continuous: bool,
    },
    /// Store a travelynx API secret in a secret service
    StoreSecret {
        /// API Secret to store
        #[arg(short, long)]
        secret: Option<String>,
    },
}

#[derive(Parser)]
#[command(author, version, about)]
struct App {
    #[clap(flatten)]
    global_args: GlobalArgs,
    /// Command to run
    #[clap(subcommand)]
    command: Command,
}

fn run_client(client: impl travelynx::Client, continuous: bool) {
    match continuous {
        true => {
            main_loop(client);
        }
        false => {
            let status = client.get();
            match status {
                Ok(value) => println!("{}", value.serialize()),
                Err(value) => println!("{}", value.serialize()),
            }
        }
    }
}

impl App {
    fn run(self) {
        match self.command {
            Command::Status { secret, file_secret, continuous } => {
                let client = match (secret, file_secret) {
                    (None, None) => panic!("Need at least one of 'secret' or 'file_secret'"),
                    (None, Some(path)) => {
                        let secret = std::fs::read_to_string(path).unwrap();
                        travelynx::NetworkClient::new(secret)
                    },
                    (Some(secret), None) => travelynx::NetworkClient::new(secret),
                    (Some(secret), Some(_)) => travelynx::NetworkClient::new(secret),
                };
                run_client(client, continuous)
            }
            Command::StoreSecret { secret: _ } => {
                todo!()
            }
            Command::Demo { file, continuous } => {
                let sample = travelynx::Status::from_file(&file.unwrap_or(String::from(""))).expect("Error reading file");
                log::debug!("Sample data is {:#?}", sample);
                let client = travelynx::MockClient::new(sample);
                run_client(client, continuous)
            }
        }
    }
}

fn main() {
    env_logger::init();
    let app = App::parse();
    app.run();
}
