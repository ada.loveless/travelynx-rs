use crate::travelynx;
use chrono::{DateTime, Local};
use std::time::SystemTime;

impl crate::waybar::StatusMessage for travelynx::Status {
    fn text(&self) -> String {
        if self.checked_in {
            format!("{} | Destination: {} | {}", self.train, self.to_station, self.next_stop())
        } else {
            String::from("Not checked in :(")
        }
    }

    fn alt(&self) -> String {
        if self.checked_in {
            format!("{}", self.to_station)
        } else {
            String::from(":(")
        }
    }

    fn tooltip(&self) -> String {
        if self.checked_in {
            let mut out = format!("{:<12}{:<12}{}\n", "Arrival", "Departure", "Stop");
            for stop in &self.timetable() {
                out.push_str(&format!("{:#}\n", stop));
            }
            out
        } else {
            let delta_str = match self.to_station.time() {
                None => String::new(),
                Some(time) => {
                    match time.elapsed() {
                        Ok(dur) => {
                            match dur.as_secs() {
                                0..=60 => format!("{}s ago", dur.as_secs()),
                                61..=3600 => format!("{}min ago", dur.as_secs() / 60),
                                _ => format!("{}h ago", dur.as_secs() / 60 / 60)
                            }
                        },
                        Err(err) => {
                            log::error!("Encountered error formatting: {}", err);
                            String::new()
                        },
                    }
                },
            };
            format!(
                "Previously on: {} from {:#} to {:#} {}",
                self.train, 
                self.from_station.name.clone().unwrap_or(String::from("Unknown")),
                self.to_station.name.clone().unwrap_or(String::from("Unknown")),
                delta_str
            )
        }
    }

    fn percentage(&self) -> Option<f64> {
        if self.checked_in {
            let from: Option<DateTime<Local>> = self.from_station.time().map(|x| x.into());
            let to: Option<DateTime<Local>> = self.to_station.time().map(|x| x.into());
            let now: DateTime<Local> = SystemTime::now().into();
            match (from, to) {
                (None, None) => None,
                (Some(_), None) => None,
                (None, Some(_)) => None,
                (Some(from), Some(to)) => {
                    let current = now - from;
                    let to = to - from;
                    Some(100.0 * (current.num_seconds() as f64) / (to.num_seconds() as f64))
                }
            }
        } else {
            None
        }
    }

    fn class(&self) -> Option<String> {
        if self.checked_in {
            None
        } else {
            Some(String::from("inactive"))
        }
    }
}
