use crate::error::{Error, Result};
use chrono::{DateTime, Local};
use serde::{Deserialize, Serialize};
use serde_with::{serde_as, TimestampSeconds};
use std::fmt::Display;
use std::time::SystemTime;

#[serde_as]
#[derive(Deserialize, Serialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Station {
    pub name: Option<String>,
    pub ds100: Option<String>,
    pub latitude: Option<f64>,
    pub longitude: Option<f64>,
    #[serde_as(as = "Option<TimestampSeconds<i64>>")]
    pub real_time: Option<SystemTime>,
    #[serde_as(as = "Option<TimestampSeconds<i64>>")]
    pub scheduled_time: Option<SystemTime>,
    pub uic: Option<u64>,
}

impl Station {
    pub fn time(&self) -> Option<SystemTime> {
        match (self.real_time, self.scheduled_time) {
            (None, None) => None,
            (None, Some(time)) => Some(time),
            (Some(time), None) => Some(time),
            (Some(time), Some(_)) => Some(time),
        }
    }
}

impl Display for Station {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut out = String::new();
        match &self.name {
            Some(name) => out.push_str(name),
            None => {}
        }
        let time = format_time_until(self.real_time, self.scheduled_time);
        let time_until = match time {
            None => todo!(),
            Some(time) => {
                time
            }
        };
        out.push_str(&format!(" in {}", time_until));
        write!(f, "{}", out)
    }
}

#[serde_as]
#[derive(Deserialize, Serialize, Debug, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Stop {
    pub name: String,
    #[serde_as(as = "Option<TimestampSeconds<i64>>")]
    pub scheduled_arrival: Option<SystemTime>,
    #[serde_as(as = "Option<TimestampSeconds<i64>>")]
    pub real_arrival: Option<SystemTime>,
    #[serde_as(as = "Option<TimestampSeconds<i64>>")]
    pub scheduled_departure: Option<SystemTime>,
    #[serde_as(as = "Option<TimestampSeconds<i64>>")]
    pub real_departure: Option<SystemTime>,
}

impl From<Station> for Stop {
    fn from(value: Station) -> Stop {
        Stop {
            name: value.name.as_ref().unwrap_or(&String::from("???")).clone(),
            scheduled_arrival: value.scheduled_time,
            real_arrival: value.real_time,
            scheduled_departure: value.scheduled_time,
            real_departure: value.real_time,
        }
    }
}

#[derive(Debug)]
pub enum StopState {
    Past,
    Present,
    Future,
    Unknown,
}

impl Stop {
    pub fn clear_arrival(&mut self) {
        self.scheduled_arrival = None;
        self.real_arrival = None;
    }
    pub fn clear_departure(&mut self) {
        self.scheduled_departure = None;
        self.real_departure = None;
    }
    fn best_arrival_time(&self) -> Option<SystemTime> {
        match (self.real_arrival, self.scheduled_arrival) {
            (Some(time), None) => Some(time),
            (None, None) => None,
            (None, Some(time)) => Some(time),
            (Some(time), Some(_)) => Some(time),
        }
    }
    fn best_departure_time(&self) -> Option<SystemTime> {
        match (self.real_departure, self.scheduled_departure) {
            (Some(time), None) => Some(time),
            (None, None) => None,
            (None, Some(time)) => Some(time),
            (Some(time), Some(_)) => Some(time),
        }
    }
    pub fn state(&self) -> StopState {
        let now = SystemTime::now();
        match (self.best_arrival_time(), self.best_departure_time()) {
            (None, None) => StopState::Unknown,
            (None, Some(time)) => {
                if time > now {
                    StopState::Present
                } else {
                    StopState::Past
                }
            }
            (Some(time), None) => {
                if time > now {
                    StopState::Future
                } else {
                    StopState::Past
                }
            }
            (Some(arrival), Some(departure)) => {
                if arrival > now {
                    StopState::Future
                } else if departure < now {
                    StopState::Past
                } else {
                    StopState::Present
                }
            }
        }
    }
}

fn format_delay(
    real: Option<SystemTime>,
    schedule: Option<SystemTime>,
) -> Option<chrono::Duration> {
    match (real, schedule) {
        (Some(real), Some(schedule)) => {
            let real: DateTime<Local> = real.into();
            let schedule: DateTime<Local> = schedule.into();
            Some(real - schedule)
        }
        _ => None,
    }
}

fn get_time(real: Option<SystemTime>, schedule: Option<SystemTime>) -> Option<DateTime<Local>> {
    match (real, schedule) {
        (Some(time), None) => Some(time),
        (None, None) => None,
        (None, Some(time)) => Some(time),
        (Some(time), Some(_)) => Some(time),
    }
    .map(|x| x.into())
}

fn format_time_pair(real: Option<SystemTime>, schedule: Option<SystemTime>) -> String {
    let time = get_time(real, schedule).map(|time| {
        let local: chrono::DateTime<Local> = time;
        local.format("%H:%M").to_string()
    });
    let delay = format_delay(real, schedule).map(|x| x.num_minutes());
    // Result is HH:MM(+000)
    match (time, delay) {
        (None, None) => String::new(),
        (None, Some(delay)) => format!("     ({:+3})", delay),
        (Some(time), None) => format!("{}     ", time),
        (Some(time), Some(delay)) => format!("{}({:+3})", time, delay),
    }
}

fn format_time_until(real: Option<SystemTime>, schedule: Option<SystemTime>) -> Option<String> {
    let now: DateTime<Local> = SystemTime::now().into();
    let time_until = get_time(real, schedule).map(|x| x - now).map(|x| {
        let secs = x.num_seconds();
        let part_min = secs / 60;
        let part_sec = secs.abs() % 60;
        format!("{:02}:{:02}", part_min, part_sec)
    });
    let delay = format_delay(real, schedule).map(|x| x.num_minutes());
    match (time_until, delay) {
        (None, None) => None,
        (None, Some(delay)) => Some(format!("{:+}min", delay)),
        (Some(time_until), None) => Some(format!("{}min", time_until)),
        (Some(time_until), Some(delay)) => Some(format!("{} ({:+})", time_until, delay)),
    }
    // Result is HH:MM+000
}

impl Display for Stop {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match f.alternate() {
            false => {
                let state = self.state();
                log::debug!("State is {state:?}");
                match state {
                    StopState::Past => write!(f, "Previous: {}", self.name)?,
                    StopState::Present => write!(f, "Current: {}", self.name)?,
                    StopState::Future => write!(f, "Next: {}", self.name)?,
                    StopState::Unknown => write!(f, "Next: {}", self.name)?,
                }
                Ok(())
            }
            true => {
                let arrival = format_time_pair(self.real_arrival, self.scheduled_arrival);
                let departure = format_time_pair(self.real_departure, self.scheduled_departure);
                write!(f, "{:<12}{:<12}{}", arrival, departure, self.name)?;
                Ok(())
            }
        }
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Train {
    //type: String,
    pub line: Option<String>,
    pub no: Option<String>,
    pub id: Option<String>,
    pub hafas_id: Option<String>,
    #[serde(rename = "type")]
    pub typ: String,
}

impl Display for Train {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let empty = String::from("");
        let line = self.line.as_ref().unwrap_or(&empty);
        write!(f, "{}{}", self.typ, line)?;
        match &self.no {
            Some(num) => write!(f, " ({})", num)?,
            None => write!(f, "")?,
        }
        Ok(())
    }
}

#[derive(Deserialize, Serialize, Debug, Default, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Visibility {
    pub desc: String,
    pub level: i64,
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub deprecated: bool,
    pub action_time: u64,
    pub checked_in: bool,
    pub comment: Option<String>,
    pub from_station: Station,
    pub to_station: Station,
    pub intermediate_stops: Vec<Stop>,
    pub train: Train,
    pub visibility: Visibility,
}

impl Status {
    pub fn from_file(file: &str) -> Result<Self> {
        let contents = std::fs::read_to_string(file).map_err(Error::IO)?;
        log::debug!("Read: {}", contents);
        let value = serde_json::from_str(&contents).map_err(Error::Parse)?;
        Ok(value)
    }

    pub fn next_stop(&self) -> Stop {
        let now = SystemTime::now();
        for stop in &self.timetable() {
            log::debug!("Checking {:?}", stop);
            match stop.best_departure_time() {
                Some(time) => {
                    if now <= time {
                        log::debug!("Next stop is {:?}", stop);
                        return stop.clone();
                    }
                }
                None => continue,
            };
        }
        self.to_station.clone().into()
    }

    pub fn timetable(&self) -> Vec<Stop> {
        let mut table = Vec::new();
        let mut from: Stop = self.from_station.clone().into();
        from.clear_arrival();
        table.push(from);
        for stop in &self.intermediate_stops {
            table.push(stop.clone());
        }
        let mut to: Stop = self.to_station.clone().into();
        to.clear_departure();
        table.push(to);
        table
    }
}

static ENDPOINT: &str = "https://travelynx.de/api/v1/status";

pub trait Client: std::fmt::Debug {
    fn get(&self) -> Result<Status>;
}

#[derive(Debug)]
pub struct NetworkClient {
    client: reqwest::blocking::Client,
    endpoint: String,
}

impl NetworkClient {
    pub fn new(secret: String) -> Self {
        let client = reqwest::blocking::Client::builder()
            .user_agent("curl/7.87.0")
            .build()
            .unwrap();
        let endpoint = format!("{}/{}", ENDPOINT, secret);
        NetworkClient { client, endpoint }
    }
}

impl Client for NetworkClient {
    fn get(&self) -> Result<Status> {
        log::info!("Getting status from {}", self.endpoint);
        let response = self
            .client
            .get(&self.endpoint)
            .send()
            .map_err(Error::Network)?;
        let content = response.text().map_err(Error::Network)?;
        let status = serde_json::from_str(&content).map_err(Error::Parse)?;
        Ok(status)
    }
}

#[derive(Debug)]
pub struct MockClient {
    data: Status,
}

impl MockClient {
    pub fn new(data: Status) -> Self {
        MockClient { data }
    }
}

impl Client for MockClient {
    fn get(&self) -> Result<Status> {
        Ok(self.data.clone())
    }
}
