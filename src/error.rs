#[derive(Debug)]
pub enum Error {
    Parse(serde_json::Error),
    IO(std::io::Error),
    Network(reqwest::Error),
}
pub type Result<T> = std::result::Result<T, Error>;

impl crate::waybar::StatusMessage for Error {
    fn text(&self) -> String {
        match self {
            Error::Parse(_) => String::from("Parse Error"),
            Error::IO(_) => String::from("I/O Error"),
            Error::Network(_) => String::from("Networking Error"),
        }
    }
    fn alt(&self) -> String {
        match self {
            Error::Parse(_) => String::from("E: Parse"),
            Error::IO(_) => String::from("E: I/O"),
            Error::Network(_) => String::from("E: Network"),
        }
    }
    fn tooltip(&self) -> String {
        match self {
            Error::Parse(err) => {
                format!("{}", err)
            },
            Error::IO(err) => {
                format!("{}", err)
            },
            Error::Network(err) => {
                format!("{}", err)
            }
        }
    }
    fn percentage(&self) -> Option<f64> {
        None
    }
    fn class(&self) -> Option<String> {
        Some(String::from("error"))
    }
}

