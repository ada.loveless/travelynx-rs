# Travelynx Status thing

For putting your travelynx status into your waybar:

![Screenshot of the travelynx status in a waybar](img.png)

## Usage

The `travelynx` command can handle the thing:

```
Usage: travelynx <COMMAND>

Commands:
  status        Status command running with the travelynx API
  demo          Demo mode based on a pre-defined json
  store-secret  Store a travelynx API secret in a secret service
  help          Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```

The `status` command will print a json formatted for waybar:

```
Status command running with the travelynx API

Usage: travelynx status [OPTIONS]

Options:
  -s, --secret <SECRET>  API Secret to use
  -c, --continuous       Run continuously
  -h, --help             Print help
```

Using the `-c` option will cause the command to run continuously. The tool will
then print a new message for waybar every second, and fetch a new travelynx
status periodically, typically every minute.
Without `-c`, the tool will run once and then quit.

To use this with waybar, place something like this n your waybar config:

```json
"custom/train_status": {
    "exec": "travelynx status -c -s {YOUR_TRAVELYNX_SECRET}",
    "return-type": "json",
    "tooltip": true
},
```

The other commands do:

- `demo` to allow you to use a pre-defined json for testing the tool.
- `store-secret`, not yet implemented, to store a travelynx secret in a secret
  storage thing (forgot the name, sorry).


